package br.ifsc.edu.br.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    EditText edlogin, edsenha;
    Button bntCriarLogin,bntLogin;
    private FirebaseAuth mAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        edlogin= findViewById(R.id.EditTextLogin);
        edsenha=findViewById(R.id.EditTextSenha);
        bntCriarLogin=findViewById(R.id.bntcria);
        bntLogin=findViewById(R.id.bntlogin);




        //Adicionando evento (listener) on click ao botão criar cadastro
        bntCriarLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cria_cadastro( view);
            }
        });

        //Conexão com firebase
        //FirebaseApp.initializeApp(getApplicationContext());
        mAuth = FirebaseAuth.getInstance();


    }

    @Override
    protected void onStart() {
        super.onStart();
        ajusta_botao_login_logout();
    }

    public void ajusta_botao_login_logout(){
        FirebaseUser user =mAuth.getCurrentUser();
        if (user == null ){
            Toast.makeText(this,"Nenhum usuário logado",Toast.LENGTH_LONG).show();
            bntLogin.setText( "Login");
        }else{
            Toast.makeText(this,user.getEmail(),Toast.LENGTH_LONG).show();
            bntLogin.setText( "Logout");

        }
    }

    public void verifica_login(View view) {
        FirebaseUser user =mAuth.getCurrentUser();
        if (user == null ) {
             mAuth.signInWithEmailAndPassword(edlogin.getText().toString(), edsenha.getText().toString());
             ajusta_botao_login_logout();

        }else{
             mAuth.signOut();
        }


    }

    public void cria_cadastro(View view) {
        mAuth.createUserWithEmailAndPassword(edlogin.getText().toString(), edsenha.getText().toString());

    }
}
